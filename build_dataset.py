
# Routine for preparing train, val and test data folders

import os
import argparse
import matplotlib.pyplot as plt
from PIL import Image
import pandas as pd
from tqdm import tqdm
import numpy as np
import random
import shutil


IMG_EXT = '.jpg'
DICT_FOLDERS_RATIOS = {'train':0.6, 'eval':0.2, 'test':0.2}
TARGET_IMG_SIZES = [512]
parser = argparse.ArgumentParser()
parser.add_argument('--source_data_dir', default='/home/mostafa/Downloads/training_challenge/images', help="Directory with the dog dataset")
parser.add_argument('--csv_filename', default='/home/mostafa/Downloads/training_challenge/labels.csv', help="csv of filenames and labels")



def generate_lists_of_images_labels(csv_filename):
    df = pd.read_csv(csv_filename, delimiter=',')
    image_names=df['image'].tolist()
    labels = df['dog'].tolist()
    return image_names, labels

def resize_and_save(image_name, data_dir, output_dir, size=256):
    """Resize the image contained in `filename` and save it to the `output_dir`"""
    image = Image.open(os.path.join(data_dir, image_name + IMG_EXT))
    # resize the image
    image = image.resize((size, size), Image.BILINEAR)
    image.save(os.path.join(output_dir, image_name + IMG_EXT))

if __name__ == '__main__':
    args = parser.parse_args()

    # generate list of filenames and labels from the provided CSV file:
    image_names, labels = generate_lists_of_images_labels(args.csv_filename)
    # shuffle the names and lists together:
    c = list(zip(image_names, labels))
    random.seed(200)
    random.shuffle(c)
    image_names, labels = zip(*c)
    

    for size in TARGET_IMG_SIZES:
        num_imgs_copied = 0
        for folder in DICT_FOLDERS_RATIOS.keys():
            target_folder = '/'.join(args.source_data_dir.split('/')[:-1])
            target_folder = os.path.join(target_folder, str(size), folder)

            N = int(DICT_FOLDERS_RATIOS[folder] * len(image_names))
            for i in tqdm(range(num_imgs_copied, num_imgs_copied+N)):

                if labels[i] == 1:
                    output_dir = os.path.join(target_folder, '1')
                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)
                else:
                    output_dir = os.path.join(target_folder, '0')
                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)

                resize_and_save(image_names[i], args.source_data_dir, output_dir, size)

            num_imgs_copied=num_imgs_copied+N
            

    print("Done building dataset")